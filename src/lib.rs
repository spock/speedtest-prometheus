
pub mod speedtest {
    use serde::{Deserialize, Serialize};
    #[derive(Serialize, Deserialize, Debug)]
    pub struct Latency {
        pub iqm: f64,
        pub low: f64,
        pub high: f64,
        pub jitter: f64,
    }
    #[derive(Serialize, Deserialize, Debug)]
    pub struct Ping {
        pub jitter: f64,
        pub latency: f64,
        pub low: f64,
        pub high: f64,
    }
    
    #[derive(Serialize, Deserialize, Debug)]
    pub struct Traffic {
        pub bandwidth: f64,
        pub bytes: f64,
        pub elapsed: f64,
        pub latency: Latency,
    }
    #[derive(Serialize, Deserialize, Debug)]
    pub struct Server {
        pub id: f64,
        pub host: String,
        pub port: u16,
        pub name: String,
        pub location: String,
        pub country: String,
        pub ip: String,
    }
    #[derive(Serialize, Deserialize, Debug)]
    pub struct ResultData {
        pub id: String,
        pub url: String,
        pub persisted: bool,
    }
    #[derive(Serialize, Deserialize, Debug)]
    pub struct SpeedTest {
        #[serde(rename = "type")]
        pub result_type: String,
        pub timestamp: String,
        pub ping: Ping,
        pub download: Traffic,
        pub upload: Traffic,
        #[serde(rename = "packetLoss")]
        pub packetloss: f64,
        pub isp: String,
        pub server: Server,
        pub result: ResultData,
    }
      
}
