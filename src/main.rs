use std::process::Command;
use std::str;
// use serde_json;
use log::{debug, info, warn};
use std::result::Result;

use actix_web::{get, App, HttpResponse, HttpServer, Responder};
use clap::Parser;
use lazy_static::lazy_static;
use prometheus::{register_gauge_vec, Encoder, GaugeVec};

use anyhow::anyhow;
use env_logger::Env;
use speedtest_prometheus::speedtest::*;


const VERSION: &str = env!("CARGO_PKG_VERSION");

// metryki
lazy_static! {
    //pub static ref REGISTRY: Registry = Registry::new();

    static ref SPEEDTEST_IDLE_LATENCY_LOW: GaugeVec = register_gauge_vec!(
        "speedtest_idle_latency_low",
        "SpeedTest measurement idle latency low",
        &["server_id", "name", "location"]
    ).unwrap();
    static ref SPEEDTEST_IDLE_LATENCY_HIGH: GaugeVec = register_gauge_vec!(
        "speedtest_idle_latency_high",
        "SpeedTest measurement idle latency high",
        &["server_id", "name", "location"]
    ).unwrap();
    static ref SPEEDTEST_IDLE_LATENCY: GaugeVec = register_gauge_vec!(
        "speedtest_idle_latency_iqm",
        "SpeedTest measurement idle latency average",
        &["server_id", "name", "location"]
    ).unwrap();
    static ref SPEEDTEST_DOWNLOAD_BW: GaugeVec = register_gauge_vec!(
        "speedtest_download_bandwidth",
        "SpeedTest download bandwidth",
        &["server_id", "name", "location"]
    ).unwrap();
    static ref SPEEDTEST_DOWNLOAD_BYTES: GaugeVec = register_gauge_vec!(
        "speedtest_download_bytes",
        "SpeedTest download bytes transferred",
        &["server_id", "name", "location"]
    ).unwrap();
    static ref SPEEDTEST_DOWNLOAD_ELAPSED: GaugeVec = register_gauge_vec!(
        "speedtest_download_elapsed",
        "SpeedTest download time elapsed (ms)",
        &["server_id", "name", "location"]
    ).unwrap();
    static ref SPEEDTEST_DOWNLOAD_LATENCY_IQM: GaugeVec = register_gauge_vec!(
        "speedtest_download_latency_iqm",
        "SpeedTest download latency average(iqm)",
        &["server_id", "name", "location"]
    ).unwrap();

    static ref SPEEDTEST_UPLOAD_BW: GaugeVec = register_gauge_vec!(
        "speedtest_upload_bandwidth",
        "SpeedTest upload bandwidth",
        &["server_id", "name", "location"]
    ).unwrap();
    static ref SPEEDTEST_UPLOAD_BYTES: GaugeVec = register_gauge_vec!(
        "speedtest_upload_bytes",
        "SpeedTest upload bytes transferred",
        &["server_id", "name", "location"]
    ).unwrap();
    static ref SPEEDTEST_UPLOAD_ELAPSED: GaugeVec = register_gauge_vec!(
        "speedtest_upload_elapsed",
        "SpeedTest upload time elapsed (ms)",
        &["server_id", "name", "location"]
    ).unwrap();
    static ref SPEEDTEST_UPLOAD_LATENCY_IQM: GaugeVec = register_gauge_vec!(
        "speedtest_upload_latency_iqm",
        "SpeedTest upload latency average(iqm)",
        &["server_id", "name", "location"]
    ).unwrap();
    static ref SPEEDTEST_PACKETLOSS: GaugeVec = register_gauge_vec!(
        "speedtest_packetloss",
        "SpeedTest total packet loss (percent)",
        &["server_id", "name", "location"]
    ).unwrap();
    static ref SPEEDTEST_RESULT_URL: GaugeVec  = register_gauge_vec!(
        "speedtest_result_url","Web url with result page", &["url"]).unwrap();
    static ref SPEEDTEST_SERVER_ID: GaugeVec  = register_gauge_vec!(
        "speedtest_server_id","SpeedTest server used for test", &["host", "name", "location"]).unwrap();
}


#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    /// IP to bind
    #[clap(short, long, value_name = "BINDIP", default_value = "127.0.0.1")]
    bind: String,
    // Listen port
    #[clap(short, long, value_name = "PORT", default_value = "9195")]
    port: u16,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();
    let cli = Cli::parse();
    let bind_ip = cli.bind;
    let bind_port = cli.port;
    info!("Bringing speedtest exporter {} on-line {}:{}", VERSION, bind_ip, bind_port);
    HttpServer::new(|| {
        App::new().service(metrics)
        //.route("/metrics", web::get().to(metrics))
    })
    .workers(2)
    .bind((bind_ip, bind_port))?
    .run()
    .await
}

#[get("/metrics")]
async fn metrics() -> impl Responder {
    let st = run_speedtest();
    // let st: Result<SpeedTest,serde_json::Error>  = serde_json::from_str(data);
    match st {
        Ok(j) => {
            let metrics = handle_metrics(j);
            HttpResponse::Ok().body(metrics)
        }
        Err(e) => {
            warn!("{}", e);
            HttpResponse::Ok().body("")
        }
    }
}

fn handle_metrics(data: SpeedTest) -> String {
    let v = data;
    let sid = v.server.id.to_string();
    let sname = v.server.name;
    let sloc = v.server.location;
    SPEEDTEST_IDLE_LATENCY_LOW.reset();
    SPEEDTEST_IDLE_LATENCY_LOW
        .with_label_values(&[&sid, &sname, &sloc])
        .set(v.ping.low);
    SPEEDTEST_IDLE_LATENCY_HIGH.reset();
    SPEEDTEST_IDLE_LATENCY_HIGH
        .with_label_values(&[&sid, &sname, &sloc])
        .set(v.ping.high);
    SPEEDTEST_IDLE_LATENCY.reset();
    SPEEDTEST_IDLE_LATENCY
        .with_label_values(&[&sid, &sname, &sloc])
        .set(v.ping.latency);
    // download data
    SPEEDTEST_DOWNLOAD_BW.reset();
    SPEEDTEST_DOWNLOAD_BW
        .with_label_values(&[&sid, &sname, &sloc])
        .set(v.download.bandwidth * 8.0);
    SPEEDTEST_DOWNLOAD_BYTES.reset();
    SPEEDTEST_DOWNLOAD_BYTES
        .with_label_values(&[&sid, &sname, &sloc])
        .set(v.download.bytes);
    SPEEDTEST_DOWNLOAD_ELAPSED.reset();
    SPEEDTEST_DOWNLOAD_ELAPSED
        .with_label_values(&[&sid, &sname, &sloc])
        .set(v.download.elapsed);
    SPEEDTEST_DOWNLOAD_LATENCY_IQM.reset();
    SPEEDTEST_DOWNLOAD_LATENCY_IQM
        .with_label_values(&[&sid, &sname, &sloc])
        .set(v.download.latency.iqm);
    //upload data
    SPEEDTEST_UPLOAD_BW.reset();
    SPEEDTEST_UPLOAD_BW
        .with_label_values(&[&sid, &sname, &sloc])
        .set(v.upload.bandwidth * 8.0);
    SPEEDTEST_UPLOAD_BYTES.reset();
    SPEEDTEST_UPLOAD_BYTES
        .with_label_values(&[&sid, &sname, &sloc])
        .set(v.upload.bytes);
    SPEEDTEST_UPLOAD_ELAPSED.reset();
    SPEEDTEST_UPLOAD_ELAPSED
        .with_label_values(&[&sid, &sname, &sloc])
        .set(v.upload.elapsed);
    SPEEDTEST_UPLOAD_LATENCY_IQM.reset();
    SPEEDTEST_UPLOAD_LATENCY_IQM
        .with_label_values(&[&sid, &sname, &sloc])
        .set(v.upload.latency.iqm);

    SPEEDTEST_PACKETLOSS.reset();
    SPEEDTEST_PACKETLOSS
        .with_label_values(&[&sid, &sname, &sloc])
        .set(v.packetloss);
    SPEEDTEST_RESULT_URL.reset();
    SPEEDTEST_RESULT_URL
        .with_label_values(&[&v.result.url])
        .set(1 as f64);
    SPEEDTEST_SERVER_ID.reset();
    SPEEDTEST_SERVER_ID
        .with_label_values(&[&sid, &sname, &sloc])
        .set(v.server.id);
    let mut res: String = "".to_string();
    let encoder = prometheus::TextEncoder::new();
    let mut buffer = Vec::new();
    if let Err(e) = encoder.encode(&prometheus::gather(), &mut buffer) {
        warn!("could not encode prometheus metrics: {}", e);
    };
    let res_custom = match String::from_utf8(buffer.clone()) {
        Ok(v) => v,
        Err(e) => {
            warn!("prometheus metrics could not be from_utf8'd: {}", e);
            String::default()
        }
    };
    buffer.clear();

    res.push_str(&res_custom);
    res
}

fn run_speedtest() -> anyhow::Result<SpeedTest> {
    info!("run_speedtest() - it will take ~30-40s");
    let mut cmdb = Command::new("speedtest");
    let cmd = cmdb
    .arg("--accept-license")
    .arg("-f")
    .arg("json");

    debug!("CMD: {:?}", cmd);
    //let cmd = cmd.output();
    match cmd.output() {
        Ok(r) => {
            let stdout = r.stdout;
            let data = str::from_utf8(&stdout).unwrap().to_string();
            debug!("RAW_JSON_DATA:\n{}", data);
            let j: Result<SpeedTest, serde_json::Error> = serde_json::from_str(&data);
            match j {
                Ok(v) => {
                    info!("run_speedtest(): done");
                    return Ok(v);
                }
                Err(e) => {
                    //warn!("Error: {}", e);
                    Err(anyhow!("speedtest cli error {}", e))
                }
            }
        }
        Err(e) => {
            Err(anyhow!("speedtest JSON output error {}", e))
        }
    }
}
