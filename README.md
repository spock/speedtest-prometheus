## speedtest-promehteus
### Summary
This application provide  single url endpoing /metrics that exposes speedtest measure to prometheus
Why another one ?
Speeds I want to measure are 0.5 - 2.5 Gbs. Many third party speedtest clients doesn't handle such traffic well.
Since there is official cli client that is good at such speed and it has JSON output I wanted to wrap it into reliable fire-and-forget exporter.
This is why binary official client + Rust written exporter are IMO best combination.

### Description
running GET on it will initiate automatic speedtest run and return metrics.
Keep in mind that _every_ request will spawn speedtest measurement :)
It will take ~30-40 seconds to return results so please keep to set scrape_timeout: 60s

### instalation
- App requires official speedtest cli binary installed in system (https://www.speedtest.net/pl/apps/cli)

- Rust installation required. If You don't have Rust on system run:

  `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
- Download and build binary:
  ```
  git clone https://gitlab.com/spock/speedtest-prometheus
  cd speedtest-prometheus
  cargo build --release
  sudo install -m 755 ./target/release/speedtest-prometheus /usr/local/bin

  ```
  
### Usage
Run appliation by providing bind port and ip. Default provided by --help
`speedtest-pometheus --help` 

Add job to prometheus:
```
  - job_name: 'speedtest'
    scrape_interval: 30m
    scrape_timeout: 60s
    static_configs:
      - targets: ['localhost:9195']
```
### Remarks
Application support RUST_LOG env variable for logging/debug purposes. Default level is "info"

